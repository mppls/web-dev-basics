        var clickedTime; var createdTime; var reactionTime;

        makeBox();

        document.getElementById('box').onclick=disappear;

        // Make div disappear
        function disappear(){

            clickedTime = Date.now();

            reactionTime = (clickedTime - createdTime)/1000;
            document.getElementById('time').innerHTML = reactionTime;

            this.style.display = "none";
            makeBox();

        }

        function getRandomHex() {
            return '#'+Math.floor(Math.random()*16777215).toString(16);
        }

        function getRandomTop() {
            return Math.floor(Math.random()*600) + "px";
        }

        function getRandomLeft() {
            return Math.floor(Math.random()*400) + "px";
        }

        function makeBox() {

            var randomNum  = Math.floor(Math.random()*5000);
            console.log(randomNum);

            setTimeout(function(){

                document.getElementById('box').style.display = "block";
                document.getElementById('box').style.backgroundColor = getRandomHex(); 
                document.getElementById('box').style.top = getRandomTop();
                document.getElementById('box').style.left = getRandomLeft();

                if (randomNum % 7 == 0 || randomNum % 4 == 0) {
                    document.getElementById('box').style.borderRadius = "50%";
                } else {
                    document.getElementById('box').style.borderRadius = "0%";
                }
                
                createdTime = Date.now();

            }, randomNum);

        }

